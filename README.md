# Swim templates

This repository contains templates for boards to be used with
[swim](https://gitlab.com/spade-lang/swim), the build tool for the hardware
description language [spade](https://gitlab.com/spade-lang/spade). Refer to
those repositories for information on how to get started.

## License

Licensed under either of

    Apache License, Version 2.0 (LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0)
    MIT license (LICENSE-MIT or http://opensource.org/licenses/MIT)

at your option
